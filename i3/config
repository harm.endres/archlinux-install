# i3 config file (v4)
#
# Please see http://i3wm.org/docs/userguide.html for a complete reference!
#
# This config file uses keycodes (bindsym) and was written for the QWERTY
# layout.
#
# To get a config file with the same key positions, but for your current
# layout, use the i3-config-wizard
#

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 8

focus_follows_mouse no

default_orientation auto

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# class                 border  backgr. text    indicator
client.focused          #9CCD00 #9CCD00 #f4f4f4 #9CCD00
client.focused_inactive #f46200 #f46200 #f4f4f4 #f46200
client.unfocused        #666666 #666666 #f4f4f4 #666666
client.urgent           #666666 #666666 #ffffff #666666
client.placeholder      #000000 #0c0c0c #ffffff #000000

client.background       #ffffff

new_window pixel 3

bindsym --whole-window $mod+button2 kill

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on retina/hidpi displays.

set $mod Mod4

# startup
exec_always --no-startup-id xautolock -time 3 -locker "i3lock -c 000000" &
exec setxkbmap -layout de,de -variant basic,neo -option -option grp:sclk_toggle -option grp_led:scroll

# use these keys for focus, movement, and resize directions when reaching for
# the arrows is not convenient
set $up l
set $down k
set $left j
set $right semicolon

# use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec terminator

# toggle touchpad
bindsym $mod+o exec [[ $(synclient | grep TouchpadOff | awk '{ print $3 }') == 1 ]] && synclient TouchpadOff=0 || synclient TouchpadOff=1

# monitor
bindsym $mod+Shift+d exec /home/hendres/bin/setdisplay

# Pulse Audio controls
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +1% || pactl set-sink-volume 1 +1%
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -1% || pactl set-sink-volume 1 -1%
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle

# Sreen brightness controls
bindsym XF86MonBrightnessUp exec xbacklight -inc 5
bindsym XF86MonBrightnessDown exec xbacklight -dec 5

# Media player controls
bindsym XF86AudioPlay exec playerctl play
bindsym XF86AudioPause exec playerctl pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

workspace_auto_back_and_forth yes



# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec dmenu_run
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

# change focus
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# alternatively, you can use the cursor keys:
bindsym $mod+Mod1+Left move workspace to output left
bindsym $mod+Mod1+Down move workspace to output down
bindsym $mod+Mod1+Up move workspace to output up
bindsym $mod+Mod1+Right move workspace to output right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+minus scratchpad show

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10



# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
   # These bindings trigger as soon as you enter the resize mode
   # Pressing left will shrink the window’s width.
   # Pressing right will grow the window’s width.
   # Pressing up will shrink the window’s height.
   # Pressing down will grow the window’s height.
   bindsym $left       resize shrink width 1 px or 1 ppt
   bindsym $down       resize grow height 1 px or 1 ppt
   bindsym $up         resize shrink height 1 px or 1 ppt
   bindsym $right      resize grow width 1 px or 1 ppt
   # same bindings, but for the arrow keys
   bindsym Left        resize shrink width 1 px or 1 ppt
   bindsym Down        resize grow height 1 px or 1 ppt
   bindsym Up          resize shrink height 1 px or 1 ppt
   bindsym Right       resize grow width 1 px or 1 ppt
   # back to normal: Enter or Escape
   bindsym Return mode "default"
   bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
   colors {
      background #000000
      statusline #666666
      separator #003C7E
      focused_workspace  #f4f4f4 #666666 #9CCD00
      active_workspace   #f4f4f4 #666666 #f4f4f4
      inactive_workspace #666666 #666666 #f4f4f4
      urgent_workspace   #2f343a #900000 #ffffff
      binding_mode       #2f343a #900000 #ffffff
   }
   #status_command i3status
   status_command .i3/i3status_bar.py
   tray_output primary
   #tray_output LVDS1 #primary
   mode dock

}

set $mode_system System (l) lock, (e) logout, (s) suspend, (h) hibernate, (r) reboot, (Shift+s) shutdown
mode "$mode_system" {
   bindsym l exec --no-startup-id $Locker, mode "default"
   bindsym e exec --no-startup-id i3-msg exit, mode "default"
   bindsym s exec --no-startup-id $Locker && systemctl suspend, mode "default"
   bindsym h exec --no-startup-id $Locker && systemctl hibernate, mode "default"
   bindsym r exec --no-startup-id systemctl reboot, mode "default"
   bindsym Shift+s exec --no-startup-id systemctl poweroff -i, mode "default"
   # back to normal: Enter or Escape
   bindsym Return mode "default"
   bindsym Escape mode "default"
}

bindsym $mod+End mode "$mode_system"

bindsym $mod+Control+l exec --no-startup-id $Locker, mode "default"

#######################################################################
# automatically start i3-config-wizard to offer the user to create a
# keysym-based config which used their favorite modifier (alt or windows)
#
# i3-config-wizard will not launch if there already is a config file
# in ~/.i3/config.
#
# Please remove the following exec line:
#######################################################################
#exec i3-config-wizard
