#!/usr/bin/env python
from i3pystatus import Status
from i3pystatus.updates import pacman, cower

status = Status(standalone=True)

status.register("dpms",
    format="d",
    color_disabled="#f46200",)

status.register("clock",
    format='%a %-d %b %H:%M:%S KW%V')

status.register("updates",
    format = "Updates! {count}",
    color="#f46200",
    backends = [pacman.Pacman(), cower.Cower()],)

status.register("backlight",
    format="☀ {percentage}",
    base_path="/sys/class/backlight/intel_backlight/",)

status.register("load",
    format="{avg1}",
    color="#9CCD00",
    critical_color="#f46200",)

status.register("temp",
    format="{temp:.0f}°C",
    color="#9CCD00",
    alert_color="#f46200",)

status.register("battery",
    format="{status} {remaining:%E%hh:%Mm}",
    alert=True,
    alert_percentage=5,
    status={
        "DIS": "↓",
        "CHR": "↑",
        "FULL": "✔",
    },
    color="#9CCD00",
    charging_color="9CCD00",
    full_color="9CCD00",
    critical_color="#f46200",)

status.register("network",
    interface="enp3s0",
    format_up="e ✔ {v4cidr}",
    format_down="e ⚡",
    color_up="#9CCD00",
    color_down="#f46200",)

status.register("network",
    interface="wlp3s0",
    format_up="w ✔ {essid} q:{quality}% ip:{v4cidr}",
    format_down="w: ⚡",
    dynamic_color="False",
    color_up="#9CCD00",
    color_down="#f46200",
    start_color="#9CCD00",
    end_color="#f46200",
    interval=3,)

#status.register("openvpn",
#    vpn_name="h",)

status.register("disk",
    path="/",
    format="h {percentage_used}% {avail}G",
    critical_limit=5,
    critical_color="#f46200",)

status.register("pulseaudio",
    format="♪{volume}",
    color_muted="#f46200",)

status.run()
