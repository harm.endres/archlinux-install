#!/usr/bin/env bash

# params
source /install_params
rm /install_params

#
# system
#

# users
echo "root:${install_user_password}" | chpasswd
useradd -m -g users -s /bin/bash ${install_user_name}
echo "${install_user_name}:${install_user_password}" | chpasswd
for i in video audio docker wheel
do
  gpasswd -a ${install_user_name} ${i}
done
echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

# time
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
systemctl enable systemd-timesyncd.service

# locale
echo 'KEYMAP=de-latin1-nodeadkeys' > /etc/vconsole.conf
echo 'LANG=en_US.UTF-8' > /etc/locale.conf
echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen
locale-gen

# networking
cat > /etc/systemd/network/50-wired.network <<EOF
[Match]
Name=en*

[Network]
DHCP=ipv4
EOF

cat > /etc/systemd/network/51-wireless.network <<EOF
[Match]
Name=wl*

[Network]
DHCP=ipv4
EOF

systemctl enable systemd-networkd.service
systemctl enable systemd-resolved.service

# boot
sed -i '/^HOOKS/{s/block/keyboard keymap block encrypt/}' /etc/mkinitcpio.conf
mkinitcpio -p linux

bootctl --path=/boot install

cat > /etc/pacman.d/hooks/systemd-boot.hook <<EOF
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot...
When = PostTransaction
Exec = /usr/bin/bootctl update
EOF

cat > /boot/loader/loader.conf <<EOF
default  arch
timeout  4
editor   0
EOF

cat > /boot/loader/entries/arch.conf <<EOF
title babydonthurtme
linux /vmlinuz-linux
initrd /initramfs-linux.img
options cryptdevice=UUID=$(blkid /dev/nvme0n1p3 -o value | head -n1):rootfs root=/dev/mapper/rootfs quiet rw
EOF

# TODO: publish internal mirror/packages/builds ... quick and dirty
# yaourt
cat > /tmp/yaourtbuild <<EOF
makepkg_opt='-C -c -s -i --noconfirm'
makepkg_packages=(package-query yaourt)
for i in \${makepkg_packages[@]}
do
  mkdir /tmp/\${i}
  cd /tmp/\${i}
  curl -o PKGBUILD "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=\${i}"
  makepkg \${makepkg_opt}
  cd
  rm -rf /tmp/\${i}
done
EOF

# install
su - ${install_user_name} -c "bash /tmp/yaourtbuild"
su - ${install_user_name} -s /bin/bash -c "yaourt -Sy --noconfirm ${install_packages}"

#
# user
#
# TODO: make wm adjustable
echo 'exec i3' > /home/${install_user_name}/.xinitrc
chown ${install_user_name} /home/${install_user_name}/.xinitrc
# TODO: include user.sh
user_atom_packages="atom-beautify build-tools busy-signal git-plus go-debug go-plus go-signature-statusbar hyperclick intentions language-puppet language-terraform linter linter-puppet-lint linter-ui-default split-diff terraform-fmt vim-modeline"
su - ${install_user_name} -s /bin/bash -c "apm install ${user_atom_packages}"
su - ${install_user_name} -s /bin/sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
chsh -s /usr/bin/zsh ${install_user_name}
mkdir /home/${install_user_name}/.i3
curl -s https://gitlab.com/harm.endres/archlinux-install/raw/master/i3/config > /home/${install_user_name}/.i3/config
curl -s https://gitlab.com/harm.endres/archlinux-install/raw/master/i3/i3status_bar.py > /home/${install_user_name}/.i3/i3status_bar.py
chown -R /home/${install_user_name}/.i3
# post install
echo 'localectl set-x11-keymap de pc105 nodeadkeys' > /var/tmp/todo
echo 'setxkbmap -layout de,de -variant basic,neo -option -option grp:sclk_toggle -option grp_led:scroll' >> /var/tmp/todo
