#!/usr/bin/env bash

# TODO detect efivars

install_disk_list=$(for i in $(ls /dev/{sd,vd,nv}* 2/dev/null);do echo -n "$i '' off ";done)
install_disk=$(dialog --clear --backtitle "disk" --radiolist "Select Disk:" 10 40 4 $install_disk_list 3>&1 1>&2 2>&3)
install_user_name=$(dialog --clear --inputbox "user" 0 0 "" 3>&1 1>&2 2>&3)
install_user_password=$(dialog --clear --insecure --passwordbox "password" 10 30 3>&1 1>&2 2>&3)
install_host_name=$(dialog --clear --inputbox "hostname" 0 0 "lokalhorst" 3>&1 1>&2 2>&3)

if [[ -e packages.txt ]]
then
  install_packages=$(sed '/^#/d;/^$/d' packages.txt)
else
  install_packages=$(curl -s $(dialog --inputbox "packaes.txt url" 0 0 "https://gitlab.com/harm.endres/archlinux-install/raw/master/packages.txt" 3>&1 1>&2 2>&3) | sed '/^#/d;/^$/d;:a;N;$!ba;s/\n/\ /g')
fi

if [[ ! -e install.sh ]]
then
  curl -s $(dialog --inputbox "install.sh url" 0 0 "https://gitlab.com/harm.endres/archlinux-install/raw/master/install.sh" 3>&1 1>&2 2>&3) > install.sh
fi
clear


# start
# disk stuff
wipefs -a ${install_disk}
sgdisk -z ${install_disk}
sgdisk -n 1:34:+1G -n 2::+$(( $(free -g | sed -rn 's/Mem:\s+([0-9]+)\s.*/\1/p') + 2 ))G -n 3:: -t 1:ef00 -t 2:8200 -t 3:8300 -c 1:"EFIBOOT" -c 2:"SWAP" -c 3:"OS" ${install_disk}
echo -n "${install_user_password}" | cryptsetup -c aes-xts-plain64 -y -s 512 luksFormat ${install_disk}p3
echo -n "${install_user_password}" | cryptsetup luksOpen ${install_disk}p3 rootfs

mkfs.fat -F 32 -n EFIBOOT ${install_disk}p1
mkswap -L SWAP ${install_disk}p2
# unencrypted
#mkfs.ext4 -F -L OS ${install_disk}p3
mkfs.ext4 -F -L OS /dev/mapper/rootfs

mount /dev/mapper/rootfs /mnt
mkdir /mnt/boot
mount ${install_disk}p1 /mnt/boot
swapon ${install_disk}p2

# bootstrap
# mirrorlist
curl -s "https://www.archlinux.org/mirrorlist/?country=$(curl -s https://ipinfo.io/country)&protocol=http&protocol=https&ip_version=4" | sed 's/^#Server/Server/' > /tmp/install_mirrorlist
#rankmirrors -n 6 /tmp/install_mirrorlist > /etc/pacman.d/mirrorlist
pacstrap /mnt base base-devel intel-ucode wpa_supplicant sudo efibootmgr dosfstools gptfdisk
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
genfstab -Up /mnt > /mnt/etc/fstab
echo $install_host_name > /mnt/etc/hostname
cat /etc/resolv.conf > /mnt/etc/resolv.conf

# install
echo "install_disk=\"${install_disk}\"" > /mnt/install_params
echo "install_user_name=\"${install_user_name}\"" >> /mnt/install_params
echo "install_user_password=\"${install_user_password}\"" >> /mnt/install_params
echo "install_packages=\"${install_packages}\"" >> /mnt/install_params

cp install.sh /mnt
rm -f install.sh
chmod +x /mnt/install.sh
arch-chroot /mnt bash /install.sh

# postinstall
ln -fs /usr/lib/systemd/resolv.conf /mnt/etc/resolv.conf
sync
umount -R /mnt
swapoff -a
sync

# Reboot
#systemctl reboot
